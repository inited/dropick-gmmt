#!/bin/sh
VERSION=`grep versionName app/build.gradle | cut -d'"' -f2 | sed "s/\\./_/g"`
PROJNAME=gmmt

if [ ! -z "$BUILD_NUMBER" ]; then
  # build na jenkinsu = true
  export SENTRY_PROPERTIES=./sentry.properties
  export SENTRY_RELEASE=$VERSION-$BUILD_NUMBER
  sentry-cli releases new -p $PROJNAME $SENTRY_RELEASE
  sentry-cli releases set-commits --auto $SENTRY_RELEASE
  sentry-cli releases finalize $SENTRY_RELEASE
fi

./gradlew assembleDebug
mv app/build/outputs/apk/debug/app-debug.apk $PROJNAME-$VERSION-$BUILD_NUMBER.apk
