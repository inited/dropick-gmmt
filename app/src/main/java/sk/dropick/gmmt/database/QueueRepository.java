package sk.dropick.gmmt.database;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

import sk.dropick.gmmt.model.QueueFile;

public class QueueRepository {

    private QueueFilesDao dao;
    private LiveData<List<QueueFile>> allData;

    public QueueRepository(Application application) {
        GmmtDatabase db = GmmtDatabase.getInstance(application);
        this.dao = db.queueFilesDao();
        this.allData = dao.getQueueFilesList();
    }

    public LiveData<List<QueueFile>> getAllData() {
        return allData;
    }

    public List<QueueFile> getAllData2() {
        return dao.getQueueFiles();
    }

// You must call this on a non-UI thread or your app will crash

    public void insert(QueueFile QueueFile) {
        new insertAsyncTask(dao).execute(QueueFile);
    }

    private static class insertAsyncTask extends AsyncTask<QueueFile, Void, Void> {
        private QueueFilesDao mAsyncTaskDao;
        insertAsyncTask(QueueFilesDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final QueueFile... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    public void delete(QueueFile QueueFile) {
        new deleteAsyncTask(dao).execute(QueueFile);
    }

    private static class deleteAsyncTask extends AsyncTask<QueueFile, Void, Void> {
        private QueueFilesDao mAsyncTaskDao;
        deleteAsyncTask(QueueFilesDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final QueueFile... params) {
            mAsyncTaskDao.delete(params[0]);
            return null;
        }
    }

    public void deleteById(String idItem) {
        new deleteByIdAsyncTask(dao).execute(idItem);
    }

    private static class deleteByIdAsyncTask extends AsyncTask<String, Void, Void> {
        private QueueFilesDao mAsyncTaskDao;
        deleteByIdAsyncTask(QueueFilesDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final String... params) {
            mAsyncTaskDao.deleteById(params[0]);
            return null;
        }
    }

}
