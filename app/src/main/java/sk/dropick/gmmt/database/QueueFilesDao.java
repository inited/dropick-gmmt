package sk.dropick.gmmt.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import sk.dropick.gmmt.model.QueueFile;

@Dao
public interface QueueFilesDao {

    @Query("SELECT * FROM QueueFiles")
    LiveData<List<QueueFile>> getQueueFilesList();

    @Insert
    void insert(QueueFile queueFile);

    @Delete
    void delete(QueueFile queueFile);

    @Query("DELETE FROM QueueFiles WHERE filename = :id")
    void deleteById(String id);

    @Query("SELECT * FROM QueueFiles")
    List<QueueFile> getQueueFiles();

}
