package sk.dropick.gmmt.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import sk.dropick.gmmt.model.QueueFile;

@Database(entities = QueueFile.class, exportSchema = false, version = 2)
public abstract class GmmtDatabase extends RoomDatabase {

    private static final String DB_NAME = "gmmt";
    private static GmmtDatabase instance;

    public static synchronized GmmtDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(), GmmtDatabase.class, DB_NAME)
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }

    public abstract QueueFilesDao queueFilesDao();
}
