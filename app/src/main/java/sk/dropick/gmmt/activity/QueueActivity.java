package sk.dropick.gmmt.activity;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import android.widget.Switch;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import sk.dropick.gmmt.R;
import sk.dropick.gmmt.adapter.QueueAdapter;
import sk.dropick.gmmt.component.CheckBoxTriStates;
import sk.dropick.gmmt.model.QueueFile;
import sk.dropick.gmmt.model.QueueViewModel;
import sk.dropick.gmmt.service.BackgroundService;
import sk.dropick.gmmt.service.FileService;

public class QueueActivity extends AppCompatActivity {
    @BindView(R.id.recyclerViewFiles)
    RecyclerView filesList;
    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.btnSendAll)
    Button btnSendAll;
    @BindView(R.id.sendToolbar)
    View sendToolbar;
    @BindView(R.id.cbSelectAll)
    CheckBoxTriStates cbSelectAll;


    Switch swBackground;

    private final static String TAG = "GMMT";

    private List<QueueFile> filesToDisplay;
    private QueueAdapter fileAdapter;

    private Context ctx;
    private QueueViewModel queueViewModel;
    private FileService fileService;

    private boolean backgroundEnabled = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_queue);
        ButterKnife.bind(this);
        ctx = this;

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.custom_toolbar);

        fileService = new FileService();

        backgroundEnabled = isServiceRunningInForeground(this, BackgroundService.class);

        queueViewModel = new ViewModelProvider(this).get(QueueViewModel.class);

        // list of files uses recycler and adapter
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        filesList.setLayoutManager(layoutManager);
        filesToDisplay = new ArrayList<>();


        fileAdapter = new QueueAdapter(filesToDisplay);
        fileAdapter.setOnItemClickListener((position, v) -> {
            QueueFile uf = filesToDisplay.get(position);

            if ("SEND".equals(v.getTag())) {
                uploadFile(uf);
                return;
            }

            if ("CHECK".equals(v.getTag())) {
                CheckBox cb = (CheckBox)v;
                queueViewModel.selectOneFile(uf.filename, cb.isChecked());
                cbSelectAll.setState(queueViewModel.getSelectedStatus());
                return;
            }
        });
        fileAdapter.setOnItemLongClickListener((position, v) -> {
            QueueFile uf = filesToDisplay.get(position);
            Log.i(TAG, "onItemLongClickListener: " + uf.filename);

            DialogInterface.OnClickListener dialogClickListener = (dialog, which) -> {
                if (which == DialogInterface.BUTTON_POSITIVE) {
                    queueViewModel.delete(uf);
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
            builder
                    .setMessage("Would you like to delete the file?")
                    .setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener)
                    .show();
        });

        filesList.setAdapter(fileAdapter);

        btnSendAll.setOnClickListener(view -> {

            showAlert("Info", "Ve fronte je: " + queueViewModel.getSelectedFiles().size() + " veci", null);
            uploadFiles(queueViewModel.getSelectedFiles());
        });


        progress.setVisibility(View.INVISIBLE);
        queueViewModel.getAllData().observe(this, new Observer<List<QueueFile>>() {
            @Override
            public void onChanged(List<QueueFile> queueFiles) {

                if (filesToDisplay == null) {
                    filesToDisplay = new ArrayList<>();
                }
                filesToDisplay.clear();
                filesToDisplay.addAll(queueFiles);

                if (fileAdapter != null) {
                    fileAdapter.notifyDataSetChanged();
                }

            }
        });

        swBackground = getSupportActionBar().getCustomView().findViewById(R.id.swBackground);
        swBackground.setChecked(backgroundEnabled);
        swBackground.setOnClickListener(view -> {
            updateUISendBackground(swBackground.isChecked());
            if (swBackground.isChecked()) {
                startService();
            } else {
                stopService();
            }
        });
        cbSelectAll.setOnClickListener(view -> {
            queueViewModel.selectAllFiles(cbSelectAll.getState() == 1);
        });
        cbSelectAll.setState(0);
        updateUISendBackground(backgroundEnabled);

    }


    private void startService() {

        Intent myService = new Intent(this, BackgroundService.class);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Log.i(TAG, "startForegroundService");
            startForegroundService(myService);
        } else {
            Log.i(TAG, "startService");
            startService(myService);
        }
    }

    private void stopService() {
        Intent serviceIntent = new Intent(this, BackgroundService.class);
        stopService(serviceIntent);
    }




    private void uploadFile(QueueFile queueFile) {

        // upload file to server
        // all network activities must be on different thread
        (new AsyncTask<String, Void, Integer>(){

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            protected Integer doInBackground(String... params) {
                return fileService.uploadFile(new File(fileService.getBackupFolder(), queueFile.filename).getAbsolutePath(), queueFile.hash, queueFile.created);
            }

            @Override
            protected void onPostExecute(Integer returnCode) {
                super.onPostExecute(returnCode);
                progress.setVisibility(View.GONE);
                if (returnCode == 200) {
                    fileService.deleteBackupFile(queueFile.filename);
                    queueViewModel.delete(queueFile);
                    showAlert("Info", "Data sent to server", null);
                    return;
                }
                showAlert("Error", "Cannot send data to server", null);
            }

        }).execute();


    }




    private void uploadFiles(List<QueueFile> queueFiles) {

        // upload file to server
        // all network activities must be on different thread
        (new AsyncTask<String, QueueFile, Integer>(){

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            protected Integer doInBackground(String... params) {
                while(!queueFiles.isEmpty()) {
                    QueueFile queueFile = queueFiles.get(0);
                    Integer result = fileService.uploadFile(new File(fileService.getBackupFolder(), queueFile.filename).getAbsolutePath(), queueFile.hash, queueFile.created);
                    if (result == 200) {
                        queueFiles.remove(0);
                        publishProgress(queueFile);
                    } else {
                        return result;
                    }
                }
                return 200;
            }

            @Override
            protected void onProgressUpdate(QueueFile... values) {
                super.onProgressUpdate(values);
                QueueFile uf = values[0];
                fileService.deleteBackupFile(uf.filename);
                queueViewModel.delete(uf);
            }

            @Override
            protected void onPostExecute(Integer returnCode) {
                super.onPostExecute(returnCode);
                progress.setVisibility(View.GONE);
                if (returnCode == 200) {
                    showAlert("Info", "Data sent to server", null);
                    return;
                }
                showAlert("Error", "Cannot send data to server", null);
            }

        }).execute();


    }



    private void updateUISendBackground(boolean backgroundEnabled) {

        sendToolbar.setVisibility(backgroundEnabled ? View.GONE : View.VISIBLE);
        fileAdapter.setBackgroundEnabled(backgroundEnabled);
        fileAdapter.notifyDataSetChanged();
    }


    private interface ClickListener {
        void onClick();
    }

    private void showAlert(String title, String msg, QueueActivity.ClickListener onClickListener) {
        AlertDialog alert = new AlertDialog.Builder(this).create();
        alert.setTitle(title);
        alert.setMessage(msg);
        alert.setButton(Dialog.BUTTON_POSITIVE, "OK", (dialog, which) -> {
            if (onClickListener != null) {
                onClickListener.onClick();
            }
        });

        alert.show();
    }

    /**
     * Check if service is running or not
     */
    private boolean isServiceRunningInForeground(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                if (service.foreground) {
                    return true;
                }
            }
        }
        return false;
    }

}