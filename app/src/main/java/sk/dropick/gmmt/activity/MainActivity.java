package sk.dropick.gmmt.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import net.lingala.zip4j.exception.ZipException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import sk.dropick.gmmt.R;
import sk.dropick.gmmt.adapter.FileAdapter;
import sk.dropick.gmmt.database.GmmtDatabase;
import sk.dropick.gmmt.model.FileToSign;
import sk.dropick.gmmt.model.QueueFile;
import sk.dropick.gmmt.service.FileService;
import io.sentry.Sentry;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.layoutPassword)
    LinearLayout passwordLayout;
    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.editTextPassword)
    EditText password;
    @BindView(R.id.buttonUnlock)
    Button btnUnlock;
    @BindView(R.id.recyclerViewFiles)
    RecyclerView filesList;
    @BindView(R.id.buttonFinish)
    FloatingActionButton btnFinish;
    @BindView(R.id.buttonSave)
    FloatingActionButton btnSave;

    private final static String TAG = "GMMT";
    private final static String FIRST_ZIP = "first.zip";
    private final static String FIRST_FILE_UNZIPPED_FOLDER = "first";
    private final static String DOCUMENTS_FOLDER = "Documents";
    private final static String DOCUMENTS_ZIP = "Documents.zip";
    private final static String EXPORT_DIR = "export";
    private FileService fileService;

    private final static int REQUEST_CODE = 4711;
    private final static int MY_PERMISSIONS_REQUEST_READ_STORAGE = 123;

    // constants from SignoSign documentation
    private final static int FINISH_BY_CUSTOMARCHIVING = 2021;
    private final static int FINISH_BY_ARCHIV = 2020;
    private final static int FINISH_BY_DROPBOX = 2025;
    private final static int FINISH_BY_SENDTOURL = 2026;
    private final static int FINISH_BY_CANCEL_FILESELECT = 1001;
    private final static int FINISH_BY_DROPBOX_LOGONERROR = 1002;
    private final static int FINISH_BY_CLOSE = 1003;

    private String inFile = "";
    private String inFilePassword = "";
    private String userPassword = "";
    private FileToSign signedFileName = null;
    private List<FileToSign> filesToDisplay;
    private FileAdapter fileAdapter;
    private String fileToSave = "";
    private String hashToSave = "";
    private long createdTimestamp = 0;

    private Context ctx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // retrieve start parameters
        Intent myIntent = getIntent();
        inFile = myIntent.getStringExtra("fileName");
        inFilePassword = myIntent.getStringExtra("filePassword");
        Uri inputURI = myIntent.getData();

        if (inputURI == null) {
            Intent queueActivityIntent = new Intent(this, QueueActivity.class);
            startActivity(queueActivityIntent);
            return;
        }


        this.ctx = this;

        // setup view controls
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        checkPermissionReadStorage();
        fileService = new FileService();

        // list of files uses recycler and adapter
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        filesList.setLayoutManager(layoutManager);
        filesToDisplay = new ArrayList<>();
        fileAdapter = new FileAdapter(filesToDisplay);
        filesList.setAdapter(fileAdapter);

        btnUnlock.setOnClickListener(v -> processDocumentsFile());
        btnFinish.setOnClickListener(v -> finishProcess());
        btnSave.setOnClickListener(v -> saveFile());
        fileAdapter.setOnItemClickListener((itemNumber, view) -> processFile(itemNumber));



        Log.i(TAG, "Input data URI:" + inputURI);

        if (inputURI != null) {

            // retrieve input file
            File firstFile = new File(getFilesDir(), FIRST_ZIP);
            try {
                receiveFileFromUri(inputURI, firstFile);
            } catch (IOException e) {
                Log.e(TAG, "Cannot receive input file", e);
                Sentry.captureException(e);
                showAlert("Error", "Cannot receive input file: " + e, null);
                return;
            }
            Log.i(TAG, "File received");

            // start processing first zip file
            processFirstFile();
        } else {
            onBackPressed();
        }
    }

    /**
     * Unpack zip file info {files}/first folder
     * There should be two files:
     * 1. Documents.zip
     * 2. Metadata.xml
     *
     * Display password dialog and wait for user
     */
    private void processFirstFile() {

        try {
            File firstFile = new File(getFilesDir(), FIRST_ZIP);
            File firstFileUnzippedFolder = new File(getFilesDir(), FIRST_FILE_UNZIPPED_FOLDER);

            // delete previous version
            fileService.deleteRecursive(firstFileUnzippedFolder);

            // extract first zip into documents folder
            fileService.extractFolder(firstFile.getAbsolutePath(), inFilePassword, firstFileUnzippedFolder.getAbsolutePath());

            // and delete input file
            fileService.deleteRecursive(firstFile);
            Log.i(TAG, "First file " + firstFile.getAbsolutePath() + " unzipped to " + firstFileUnzippedFolder.getAbsolutePath());

            progress.setVisibility(View.GONE);
            passwordLayout.setVisibility(View.VISIBLE);

        } catch (ZipException e) {
            Sentry.captureException(e);
            e.printStackTrace();
            Toast.makeText(this, e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Started when user enters password
     * Unzip Documents.zip info {files}/Documents folder
     * Display list of files and wait for user
     *
     */
    private void processDocumentsFile() {
        progress.setVisibility(View.VISIBLE);
        passwordLayout.setVisibility(View.GONE);
        try {
            userPassword = password.getText().toString();
            File firstFileUnzippedFolder = new File(getFilesDir(), FIRST_FILE_UNZIPPED_FOLDER);
            File documentsZipFile = new File(firstFileUnzippedFolder, DOCUMENTS_ZIP);

            File documentsFolder = new File(getFilesDir(), DOCUMENTS_FOLDER);
            fileService.deleteRecursive(documentsFolder);
            fileService.extractFolder(documentsZipFile.getAbsolutePath(), userPassword, documentsFolder.getAbsolutePath());
            fileService.deleteRecursive(documentsZipFile);

            File[] files = documentsFolder.listFiles();
            filesToDisplay.clear();
            if (files != null) {
                for (File file : files) {
                    Log.i(TAG, "File in zip:" + file.getName());
                    if (file.getName().toLowerCase().endsWith("pdf")) {
                        filesToDisplay.add(new FileToSign(file));
                    }
                }
            }

            progress.setVisibility(View.GONE);
            filesList.setVisibility(View.VISIBLE);
            btnFinish.show();
            btnSave.hide();

        } catch (ZipException e) {
            Sentry.captureException(e);
            Log.e(TAG, "Cannot unzip file", e);
            Toast.makeText(this, e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            progress.setVisibility(View.GONE);
            passwordLayout.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Start signing process when user clicks on file list
     * Create copy in external files folder and
     * start SignoSign
     *
     * @param itemNumber position in list of files
     */
    private void processFile(int itemNumber) {
        FileToSign firstFile = filesToDisplay.get(itemNumber);

        // remember it for processing result
        signedFileName = firstFile;

        // create copy of the file
        // - because of security
        // - bacause of SignoSign delete the file if signing process is canceled (click back button)
        File tempFileToSign = new File(getExternalFilesDir(null), "fileToSign.pdf");
        try {
            fileService.copyFile(firstFile.getFile(), tempFileToSign);
        } catch (IOException e) {
            Sentry.captureException(e);
            Log.e(TAG, "Cannot copy file", e);
            return;
        }

        Log.i(TAG, "Starting SignoSign for file url:" + tempFileToSign.getAbsolutePath());

        Intent i = new Intent("android.intent.action.VIEW");
        i.setType("application/pdf");
        i.putExtra("url", tempFileToSign.getAbsolutePath());
        startActivityForResult(i, REQUEST_CODE);
    }

    /**
     * Return back from SignoSign
     * Expected action is Archive Document after sign
     *
     * If success, the result code is FINISH_BY_ARCHIV and result intent contains
     * reference to signed file. Receive the file and replace file in Documents folder
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i(TAG, "on activity result requestCode:" + requestCode + ", resultCode:" + resultCode + ", data:" + data);

        if (resultCode != FINISH_BY_ARCHIV) {
            // user canceled the process
            return;
        }

        try {
            receiveFileFromUri(data.getData(), signedFileName.getFile());
        } catch (IOException e) {
            Sentry.captureException(e);
            Log.e(TAG, "Cannot receive signed file", e);
            showAlert("Error", "Cannot receive signed file: " + e, null);
            return;
        }

        // mark file as signed, so use sees the result
        signedFileName.setSigned();
        fileAdapter.notifyDataSetChanged();

        Log.i(TAG, "File received from signing:" + signedFileName.getFile().getAbsolutePath());
    }

    /**
     * Create final zip
     *
     */
    private void finishProcess() {
        try {

            File firstFileUnzippedFolder = new File(getFilesDir(), FIRST_FILE_UNZIPPED_FOLDER);

            File documentsFolder = new File(getFilesDir(), DOCUMENTS_FOLDER);

            File documentsZipFile = new File(firstFileUnzippedFolder, DOCUMENTS_ZIP);
            fileService.deleteRecursive(documentsZipFile);

            // zip PDF files into Documents.zip
            try {
                fileService.createZip(documentsFolder.getAbsolutePath(), documentsZipFile.getAbsolutePath(), userPassword);
            } catch (ZipException e) {
                Sentry.captureException(e);
                Log.e(TAG, "Cannot create Documents.zip file", e);
                return;
            }

            Log.i(TAG, "Documents.zip created");

            File exportDir = new File(getFilesDir(), EXPORT_DIR);
            fileService.deleteRecursive(exportDir);
            exportDir.mkdir();

            String resultFileName = inFile.replace("out_", "in_");
            File resultZipFile = new File(exportDir, resultFileName);
            String fileToBackup = null;

            // zip Documents.zip and Metadata.xml info in_XXXXX.zip
            try {
                fileService.createZip(firstFileUnzippedFolder.getAbsolutePath(), resultZipFile.getAbsolutePath(), inFilePassword);
                fileToBackup = fileService.copyBackupFile(resultZipFile);
            } catch (ZipException e) {
                Sentry.captureException(e);
                Log.e(TAG, "Cannot create result file", e);
                return;
            } catch (IOException e) {
                Sentry.captureException(e);
                Log.e(TAG, "Cannot create backup file", e);
                return;
            }

            // compute hash
            // hash is used to check file integrity during upload
            String hash;
            try {
                hash = computeHash(resultZipFile);
            } catch (Exception e) {
                Sentry.captureException(e);
                Log.e(TAG, "Cannot compute hash", e);
                return;
            }
            Log.i(TAG, "File hash:" + hash);

            createdTimestamp = new Date().getTime();
            Log.i(TAG, "File created timestamp:" + createdTimestamp);

            // upload file to server
            // all network activities must be on different thread
            String finalFileToBackup = fileToBackup;
            (new AsyncTask<Object, Void, Integer>(){

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    progress.setVisibility(View.VISIBLE);
                }

                @Override
                protected Integer doInBackground(Object... params) {
                    return fileService.uploadFile(resultZipFile.getAbsolutePath(), (String)params[0], (Long)params[1]);
                }

                @Override
                protected void onPostExecute(Integer returnCode) {
                    super.onPostExecute(returnCode);
                    progress.setVisibility(View.GONE);
                    if (returnCode == 200) {
                        fileService.deleteBackupFile(finalFileToBackup);
                        showAlert("Info", "Data sent to server", () -> returnBackToCallerActivity());
                        return;
                    }
                    fileToSave = finalFileToBackup;
                    hashToSave = hash;
                    btnSave.show();

                    showAlert("Error", "Cannot send data to server", null);
                }

            }).execute(hash, new Long(createdTimestamp));
        } catch (IllegalArgumentException e) {
            Sentry.captureException(e);
            Log.e(TAG, "File can't be shared", e);
        }
    }

    private void saveFile() {

        QueueFile queueFile = new QueueFile();

        queueFile.filename = fileToSave;
        queueFile.hash = hashToSave;
        queueFile.created = createdTimestamp;

        AsyncTask.execute(() -> {
            GmmtDatabase db = GmmtDatabase.getInstance(ctx);
            db.queueFilesDao().insert(queueFile);
            returnBackToCallerActivity();
        });

    }

    /**
     * Return back to caller
     *
     */
    private void returnBackToCallerActivity() {
        Intent resultIntent =  new Intent();
        setResult(200, resultIntent);
        finish();
    }

    @Override
    public void onBackPressed() {
        finishAffinity();
        System.exit(0);
    }

    /**
     * Receive file from URI
     * that is mostly result from FileProvider
     * @param uri Source URI
     * @param file Destination File
     */
    private void receiveFileFromUri(Uri uri, File file) throws IOException {

        byte[] buff = new byte[1024 * 1024 * 2]; //2MB file
        int read;

        try (InputStream in = getContentResolver().openInputStream(uri); FileOutputStream out = new FileOutputStream(file)) {
            while ((read = in.read(buff)) > 0) {
                out.write(buff, 0, read);
            }
        }
    }

    private interface ClickListener {
        void onClick();
    }

    private void showAlert(String title, String msg, ClickListener onClickListener) {
        AlertDialog alert = new AlertDialog.Builder(this).create();
        alert.setTitle(title);
        alert.setMessage(msg);
        alert.setButton(Dialog.BUTTON_POSITIVE, "OK", (dialog, which) -> {
            if (onClickListener != null) {
                onClickListener.onClick();
            }
        });

        alert.show();
    }

    private String computeHash(File file) throws NoSuchAlgorithmException, IOException {
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            Sentry.captureException(e);
        }

        // fallback if SHA-256 is not available
        if (digest == null) {
            try {
                digest = MessageDigest.getInstance("MD5");
            } catch (NoSuchAlgorithmException e) {
                Sentry.captureException(e);
                throw e;
            }
        }

        digest.reset();

        //Get file input stream for reading the file content
        FileInputStream fis = new FileInputStream(file);

        //Create byte array to read data in chunks
        byte[] byteArray = new byte[1024];
        int bytesCount;

        //Read file data and update in message digest
        while ((bytesCount = fis.read(byteArray)) != -1) {
            digest.update(byteArray, 0, bytesCount);
        }

        //close the stream; We don't need it now.
        fis.close();

        //Get the hash's bytes
        byte[] bytes = digest.digest();

        //This bytes[] has bytes in decimal format;
        //Convert it to hexadecimal format
        StringBuilder sb = new StringBuilder();
        for (byte aByte : bytes) {
            sb.append(Integer.toString((aByte & 0xff) + 0x100, 16).substring(1));
        }

        //return complete hash
        return sb.toString();
    }

    /**
     * Ask user for permissions.
     * NOTE - mind 'android:requestLegacyExternalStorage="true"' in AndroidManifest.xml
     * this is needed for Android 10+
     */
    public boolean checkPermissionReadStorage() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            // when user refused
            Toast.makeText(this, "Permission for access files is needed", Toast.LENGTH_SHORT).show();
        }

        ActivityCompat.requestPermissions(this,
                new String[] { Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE },
                MY_PERMISSIONS_REQUEST_READ_STORAGE);

        // The callback method gets the result of the request.
        return false;
    }
}
