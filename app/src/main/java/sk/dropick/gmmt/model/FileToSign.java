package sk.dropick.gmmt.model;

import java.io.File;

public class FileToSign {

    private File file;
    private boolean signed;

    public FileToSign(File file) {
        this.file = file;
        this.signed = false;
    }

    public File getFile() {
        return file;
    }

    public boolean isSigned() {
        return signed;
    }

    public void setSigned() {
        this.signed = true;
    }
}
