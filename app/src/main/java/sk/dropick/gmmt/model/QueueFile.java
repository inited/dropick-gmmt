package sk.dropick.gmmt.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "queueFiles")
public class QueueFile {

    @PrimaryKey(autoGenerate = false)
    @NonNull
    public String filename;

    @ColumnInfo(name = "hash")
    public String hash;

    @ColumnInfo(name = "created")
    public long created;

    // slouzi pouze k ovladani checkboxu u polozky v UI,
    // neuklada se do databaze
    @Ignore
    public boolean selected = false;

}
