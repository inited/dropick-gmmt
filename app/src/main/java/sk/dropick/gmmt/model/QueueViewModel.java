package sk.dropick.gmmt.model;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import sk.dropick.gmmt.database.QueueRepository;

public class QueueViewModel extends AndroidViewModel {

    private static String TAG = "GMMT";

    private QueueRepository queueRepository;
    private LiveData<List<QueueFile>> queueLiveData;
    private MediatorLiveData<List<QueueFile>> mediatorLiveData;
    private HashSet<String> selectedFiles;
    private MutableLiveData<HashSet<String>> selectedLiveData;

    public QueueViewModel(@NonNull Application application) {
        super(application);
        queueRepository = new QueueRepository(application);
        queueLiveData = queueRepository.getAllData();

        selectedLiveData = new MutableLiveData<>();

        selectedFiles = new HashSet<String>();

        mediatorLiveData = new MediatorLiveData<>();
        mediatorLiveData.addSource(queueLiveData, item -> { mediatorLiveData.setValue(item);});
        mediatorLiveData.addSource(selectedLiveData, item -> {
            List<QueueFile> vvv = mediatorLiveData.getValue();
            for(QueueFile uf : vvv) {
                uf.selected = item.contains(uf.filename);
            }
            mediatorLiveData.setValue(vvv);

        });
    }

    public LiveData<List<QueueFile>> getAllData() {
        return mediatorLiveData;
    }

    public void delete(QueueFile queueFile) {
        queueRepository.delete(queueFile);
    }

    public void selectAllFiles(boolean isSelected) {
        if (isSelected) {
            List<QueueFile> l = queueRepository.getAllData().getValue();
            for(QueueFile uf : l) {
                selectedFiles.add(uf.filename);
            }
        } else {
            selectedFiles.clear();
        }
        selectedLiveData.setValue(selectedFiles);
    }

    public void selectOneFile(String id, boolean isSelected) {
        if (isSelected) {
            selectedFiles.add(id);
        } else {
            selectedFiles.remove(id);
        }
        selectedLiveData.setValue(selectedFiles);
    }

    public int getSelectedStatus() {
        boolean anySelected = false;
        boolean anyUnselected = false;

        List<QueueFile> l = queueRepository.getAllData().getValue();
        for(QueueFile uf : l) {
            if (uf.selected) {
                anySelected = true;
            } else {
                anyUnselected = true;
            }
        }

        if (anySelected && anyUnselected) return -1;
        return (anySelected?1:0);
    }

    public List<QueueFile> getSelectedFiles() {
        List<QueueFile> l = queueRepository.getAllData().getValue();
        ArrayList<QueueFile> selectedFilesToReturn = new ArrayList<>();
        for(QueueFile uf : l) {
            if (selectedFiles.contains(uf.filename)) {
                selectedFilesToReturn.add(uf);
            }
        }
        return selectedFilesToReturn;
    }

}
