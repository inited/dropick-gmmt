package sk.dropick.gmmt.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.List;

import sk.dropick.gmmt.R;
import sk.dropick.gmmt.model.QueueFile;

public class QueueAdapter extends RecyclerView.Adapter<QueueAdapter.MyViewHolder> {

    private static ClickListener clickListener;
    private static LongClickListener longClickListener;
    final private List<QueueFile> files;
    final private SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.YYYY HH:mm:ss");
    private boolean backgroundEnabled;

    public QueueAdapter(List<QueueFile> files) {
        this.files = files;
        this.backgroundEnabled = true;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new QueueAdapter.MyViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_queue, parent, false));
    }

    @Override
    public void onBindViewHolder(QueueAdapter.MyViewHolder holder, int position) {
        QueueFile file = files.get(position);
        holder.textViewName.setText(file.filename);
        holder.textViewDateTime.setText(sdf.format(file.created));
        holder.cbSelected.setChecked(file.selected);
        holder.btnSend.setVisibility(backgroundEnabled ? View.INVISIBLE : View.VISIBLE);
        holder.cbSelected.setVisibility(backgroundEnabled ? View.INVISIBLE : View.VISIBLE);
    }

    @Override
    public int getItemCount() {
        return files.size();
    }

    public void setBackgroundEnabled(boolean backgroundEnabled) {
        this.backgroundEnabled = backgroundEnabled;
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        final TextView textViewName;
        final TextView textViewDateTime;
        final Button btnSend;
        final CheckBox cbSelected;

        MyViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            view.setLongClickable(true);
            view.setOnLongClickListener(this);
            textViewName = view.findViewById(R.id.textViewName);
            textViewDateTime = view.findViewById(R.id.textViewDatetime);
            cbSelected = view.findViewById(R.id.cbSelected);
            cbSelected.setOnClickListener(this);
            cbSelected.setTag("CHECK");
            btnSend = view.findViewById(R.id.buttonSend);
            btnSend.setOnClickListener(this);
            btnSend.setTag("SEND");
        }

        @Override
        public void onClick(View view) {
            clickListener.onItemClick(getAdapterPosition(), view);
        }


        @Override
        public boolean onLongClick(View view) {
            longClickListener.onItemLongClick(getAdapterPosition(), view);
            return true;
        }
    }


    public void setOnItemClickListener(QueueAdapter.ClickListener clickListener) {
        QueueAdapter.clickListener = clickListener;
    }

    public void setOnItemLongClickListener(QueueAdapter.LongClickListener longClickListener) {
        QueueAdapter.longClickListener = longClickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
    }

    public interface LongClickListener {
        void onItemLongClick(int position, View v);
    }

}