package sk.dropick.gmmt.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import sk.dropick.gmmt.model.FileToSign;
import sk.dropick.gmmt.R;

public class FileAdapter extends RecyclerView.Adapter<FileAdapter.MyViewHolder> {
    private static ClickListener clickListener;
    final private List<FileToSign> files;

    public FileAdapter(List<FileToSign> files) {
        this.files = files;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_file, parent, false));
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        FileToSign file = files.get(position);
        holder.textViewName.setText(file.getFile().getName());
        holder.imgIcon.setVisibility(file.isSigned()?View.VISIBLE:View.GONE);
    }

    @Override
    public int getItemCount() {
        return files.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        final TextView textViewName;
        final ImageView imgIcon;

        MyViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            textViewName = view.findViewById(R.id.textViewName);
            imgIcon = view.findViewById(R.id.imgIcon);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }
    }


    public void setOnItemClickListener(ClickListener clickListener) {
        FileAdapter.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
    }
}