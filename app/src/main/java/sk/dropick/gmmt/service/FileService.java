package sk.dropick.gmmt.service;

import android.os.Environment;
import android.util.Log;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;

import io.sentry.Sentry;


public class FileService {

    private final static String TAG= "GMMT";
    private final static String UPLOAD_URL = "https://work.pavrda.cz/upload.php";

    public void extractFolder(String filePath, String password, String dest) throws ZipException {

        ZipFile file = new ZipFile(filePath);

        if (file.isEncrypted()) {
            file.setPassword(password);
        }

        file.extractAll(dest);
    }

    public void deleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                deleteRecursive(child);

        fileOrDirectory.delete();
    }

    public void createZip(String folderPath, String destFile, String password) throws ZipException {
        Log.d(TAG, "createZip: " + folderPath + " : " + destFile);
        ZipParameters parameters = new ZipParameters();
        parameters.setEncryptFiles(true);
        parameters.setEncryptionMethod(Zip4jConstants.ENC_METHOD_STANDARD);
        parameters.setIncludeRootFolder(false);
        parameters.setPassword(password);

        File f = new File(destFile);
        if (f.exists()) {
            deleteRecursive(f);
        }
        new ZipFile(destFile).createZipFileFromFolder(folderPath, parameters, false, 0);
    }

    public File getBackupFolder() {
        File rootPath = Environment.getExternalStorageDirectory();
        File folder = new File(rootPath, "gmmt");
        if (!folder.exists()) {
            folder.mkdir();
        }
        return folder;
    }

    public String copyBackupFile(File fileToBackup) throws IOException {
        String tempFileName = new Date().getTime() + "-" + fileToBackup.getName();
        File tempFile = new File(getBackupFolder(), tempFileName);
        copyFile(fileToBackup, tempFile);
        return tempFileName;
    }

    public void deleteBackupFile(String tempFileName) {
        File tempFile = new File(getBackupFolder(), tempFileName);
        tempFile.delete();
    }

    /**
     * Copy one file to another
     * Android added this function but from Android.Q version
     * @param src Source File
     * @param dst Destination File
     */
    public void copyFile(File src, File dst) throws IOException {
        byte[] buff = new byte[1024 * 1024 * 2]; //2MB file
        int read;

        try (InputStream in = new FileInputStream(src); FileOutputStream out = new FileOutputStream(dst)) {
            while ((read = in.read(buff)) > 0) {
                out.write(buff, 0, read);
            }
        }
    }

    public int uploadFile(final String selectedFilePath, final String hash, long created){

        int serverResponseCode = 0;

        HttpURLConnection connection;
        DataOutputStream dataOutputStream;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";


        int bytesRead,bytesAvailable,bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        File selectedFile = new File(selectedFilePath);


        String[] parts = selectedFilePath.split("/");
        final String fileName = parts[parts.length-1];


            try{
                FileInputStream fileInputStream = new FileInputStream(selectedFile);
                java.net.URL url = new URL(UPLOAD_URL + "?created=" + created + "&hash=" + hash);
                connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);//Allow Inputs
                connection.setDoOutput(true);//Allow Outputs
                connection.setUseCaches(false);//Don't use a cached Copy
                connection.setConnectTimeout(15000);
                connection.setReadTimeout(15000);
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Connection", "Keep-Alive");
                connection.setRequestProperty("ENCTYPE", "multipart/form-data");
                connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                connection.setRequestProperty("uploaded_file",selectedFilePath);

                //creating new dataoutputstream
                dataOutputStream = new DataOutputStream(connection.getOutputStream());

                //writing bytes to data outputstream
                dataOutputStream.writeBytes(twoHyphens + boundary + lineEnd);
                dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""
                        + selectedFilePath + "\"" + lineEnd);

                dataOutputStream.writeBytes(lineEnd);

                //returns no. of bytes present in fileInputStream
                bytesAvailable = fileInputStream.available();
                //selecting the buffer size as minimum of available bytes or 1 MB
                bufferSize = Math.min(bytesAvailable,maxBufferSize);
                //setting the buffer as byte array of size of bufferSize
                buffer = new byte[bufferSize];

                //reads bytes from FileInputStream(from 0th index of buffer to buffersize)
                bytesRead = fileInputStream.read(buffer,0,bufferSize);

                //loop repeats till bytesRead = -1, i.e., no bytes are left to read
                while (bytesRead > 0){
                    //write the bytes read from inputstream
                    dataOutputStream.write(buffer,0,bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable,maxBufferSize);
                    bytesRead = fileInputStream.read(buffer,0,bufferSize);
                }

                dataOutputStream.writeBytes(lineEnd);
                dataOutputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                serverResponseCode = connection.getResponseCode();
                String serverResponseMessage = connection.getResponseMessage();

                Log.i(TAG, "Server Response is: " + serverResponseMessage + ": " + serverResponseCode);

                //response code of 200 indicates the server status OK
                if(serverResponseCode == 200){
                    Log.i(TAG, "Uploaded");
                }

                //closing the input and output streams
                fileInputStream.close();
                dataOutputStream.flush();
                dataOutputStream.close();



            } catch (Exception e) {
                Sentry.captureException(e);
                Log.e(TAG, "Upload error", e);
            }
            return serverResponseCode;
        }

}
