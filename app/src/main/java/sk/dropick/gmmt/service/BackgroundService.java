package sk.dropick.gmmt.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import sk.dropick.gmmt.R;
import sk.dropick.gmmt.activity.QueueActivity;
import sk.dropick.gmmt.model.QueueFile;
import sk.dropick.gmmt.database.QueueRepository;

public class BackgroundService extends Service {

    private final static String TAG = "BackgroundService";
    private final static String NOTIFICATION_TEXT = "GMMT uploader";
    private final static String NOTIFICATION_TICKER = "GMMT uploader";
    private static final String NOTIFICATION_CHANNEL_ID_SERVICE = "sk.dropick.gmmt.service";
    private static final String NOTIFICATION_CHANNEL_ID_INFO = "sk.dropick.gmmt.info";
    private final static int ONGOING_NOTIFICATION_ID = 1;
    private final static int REPEAT_INTERVAL = 15000;  // in ms

    private Context context = this;
    private Handler handler = null;
    private static Runnable runnable = null;

    private AsyncTask<String, Void, Integer> currentTask;
    private static QueueRepository queueRepository;
    private static FileService fileService;

    private NotificationCompat.Builder notificationBuilderOld;
    private Notification.Builder notificationBuilderNew;
    private boolean queueIsEmpty;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {

        Log.i(TAG, "onCreate");

        // setup notifications
        // it is required for Android 6+
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            nm.createNotificationChannel(new NotificationChannel(NOTIFICATION_CHANNEL_ID_SERVICE, "App Service", NotificationManager.IMPORTANCE_DEFAULT));
            nm.createNotificationChannel(new NotificationChannel(NOTIFICATION_CHANNEL_ID_INFO, "Download Info", NotificationManager.IMPORTANCE_DEFAULT));
        }

        // intent for start the app by click on notification
        Intent notificationIntent = new Intent(this, QueueActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            notificationBuilderNew = new Notification.Builder(this, NOTIFICATION_CHANNEL_ID_INFO)
                    .setContentTitle(getString(R.string.appName))
                    .setContentText(NOTIFICATION_TEXT)
                    .setSmallIcon(R.drawable.ic_empty_queue_black_24dp)
                    .setContentIntent(pendingIntent)
                    .setTicker(NOTIFICATION_TICKER)
                    .setOnlyAlertOnce(true)
                    .setAutoCancel(true);
            startForeground(ONGOING_NOTIFICATION_ID, notificationBuilderNew.build());

        } else {

            notificationBuilderOld = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID_INFO)
                    .setContentTitle(getString(R.string.appName))
                    .setContentText(NOTIFICATION_TEXT)
                    .setSmallIcon(R.drawable.ic_empty_queue_black_24dp)  // be sure to use bitmap icon, not vector - for 5.0.1 Android
                    .setContentIntent(pendingIntent)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setTicker(NOTIFICATION_TICKER)
                    .setOnlyAlertOnce(true)
                    .setAutoCancel(true);
            startForeground(ONGOING_NOTIFICATION_ID, notificationBuilderOld.build());
        }

        queueRepository = new QueueRepository(getApplication());
        fileService = new FileService();

        // this is the actual work to do
        handler = new Handler();
        runnable = () -> {

            // plan next run
            handler.removeCallbacks(runnable);
            handler.postDelayed(runnable, REPEAT_INTERVAL);

            uploadFile();
        };

        // run the job
        handler.postDelayed(runnable, REPEAT_INTERVAL);
        uploadFile();
    }

    @Override
    public void onDestroy() {
        handler.removeCallbacks(runnable);
        if (currentTask != null) {
            currentTask.cancel(true);
        }
    }

    private void uploadFile() {
        if (currentTask == null) {
            Log.i(TAG, "uploadFile: task==null");
            currentTask = new UploadAsyncTask(this);
            currentTask.execute("");
        }
    }

    /**
     * Network activities cannot be done on main thread
     * AsyncTask is the way how to solve it
     */
    private class UploadAsyncTask extends AsyncTask<String, Void, Integer> {

        //private BackgroundService service;
        private final WeakReference<BackgroundService> service;

        public UploadAsyncTask(BackgroundService service) {
            this.service = new WeakReference<>(service);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.i(TAG, "async task started");
            setNotificationIconActive(true);
        }

        @Override
        protected Integer doInBackground(String... params) {

            List<QueueFile> files = queueRepository.getAllData2();
            Log.i(TAG, "uploadFile: " + files);
            if ((files == null) || files.isEmpty()) {
                queueIsEmpty = true;
                return null;
            }

            List<QueueFile> shadowQueue = new ArrayList<>(files);
            for(QueueFile file: files) {
                int result = fileService.uploadFile(new File(fileService.getBackupFolder(), file.filename).getAbsolutePath(), file.hash, file.created);
                Log.i(TAG, "uploadFile status: " + result);
                if (result == 200) {
                    queueRepository.delete(file);
                    shadowQueue.remove(file);
                }
            }

            // check if queue is empty from shadow list
            // because database is processing delete command at this moment
            queueIsEmpty = shadowQueue.isEmpty();
            return null;
        }

        @Override
        protected void onPostExecute(Integer returnCode) {
            super.onPostExecute(returnCode);
            Log.i(TAG, "async task finished");
            setNotificationIconActive(false);
            currentTask = null;
        }
    }

    private void setNotificationIconActive(boolean active) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            notificationBuilderNew.setSmallIcon( active ? android.R.drawable.stat_sys_upload :
                    queueIsEmpty ? R.drawable.ic_empty_queue_black_24dp : R.drawable.ic_upload_black_24dp);

            NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            nm.notify(ONGOING_NOTIFICATION_ID, notificationBuilderNew.build());
        } else {

            notificationBuilderOld.setSmallIcon( active ? android.R.drawable.stat_sys_upload :
                    queueIsEmpty ? R.drawable.ic_empty_queue_black_24dp : R.drawable.ic_upload_black_24dp);

            NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            nm.notify(ONGOING_NOTIFICATION_ID, notificationBuilderOld.build());

        }

    }


}
