#!/bin/sh
VERSION=`grep versionName app/build.gradle | cut -d'"' -f2 | sed "s/\\./_/g"`
PROJNAME=gmmt

scp $PROJNAME-$VERSION-$BUILD_NUMBER.apk inited@ini.inited.cz:public_html/ios/
